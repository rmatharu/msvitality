<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="urn:iso:std:iso:20022:tech:xsd:pain.001.001.03"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <xsl:output method="xml" indent="yes"/>

    <!--define some address variable here, as the address in Cdtr, UltmtCdtr are same as CdtrAgt's.-->
    <!--
        For testing through cbis process, 'select' function cannot specify the mxpayment as root, otherwise cannot extract corresponding element.
        For testing directly by Oxygen, it needs the mxpayment root, eg: mxpayment/PmtInf/CdtTrfTxInf/CdtrAgt/streetName.
    -->
    <xsl:variable name="cdtrAgtStrtNm" select="PmtInf/CdtTrfTxInf/CdtrAgt/streetName"/>
    <xsl:variable name="cdtrAgtbldgNb" select="PmtInf/CdtTrfTxInf/CdtrAgt/buildingNumber"/>
    <xsl:variable name="cdtrAgtPstCd" select="PmtInf/CdtTrfTxInf/CdtrAgt/postCode"/>
    <xsl:variable name="cdtrAgtSuburb" select="PmtInf/CdtTrfTxInf/CdtrAgt/suburb"/>
    <xsl:variable name="cdtrAgtDistrict" select="PmtInf/CdtTrfTxInf/CdtrAgt/district"/>
    <xsl:variable name="cdtrAgtCtry" select="PmtInf/CdtTrfTxInf/CdtrAgt/creditorBankCountry"/>
    <xsl:variable name="cdtrAgtNm" select="PmtInf/CdtTrfTxInf/CdtrAgt/creditorBankName"/>

    <!--There are four payment types:
        BACS -  NURG,
        CHAPS - URGP,
        FASTPAY - URNS,
        INTTFR - BKTR
    -->
    <xsl:variable name="reqPaymentType" select="PmtInf/requestPaymentType"/>

    <!--Start processing the mcmapper's result sets-->
    <xsl:template match="mxpayment">
        <Document>
            <CstmrCdtTrfInitn>
                <xsl:apply-templates select="GrpHdr"/>
                <xsl:apply-templates select="PmtInf"/>
            </CstmrCdtTrfInitn>
        </Document>
    </xsl:template>

    <xsl:template match="GrpHdr">
        <GrpHdr>
            <MsgId>
                <xsl:value-of select="messageId"/>
            </MsgId>
            <CreDtTm>
                <xsl:value-of select="creationDatetime"/>
            </CreDtTm>
            <NbOfTxs>
                <xsl:value-of select="nbOfTxs"/>
            </NbOfTxs>
            <xsl:apply-templates select="InitgPty" />
        </GrpHdr>
    </xsl:template>


    <xsl:template match="InitgPty" >
        <InitgPty>
            <xsl:call-template name="PartyIdentification32">
                <xsl:with-param name="bicorbei" select="bicorbei"/>
                <xsl:with-param name="othrid" select="swiftCustomerId"/>
            </xsl:call-template>
        </InitgPty>
    </xsl:template>

    <xsl:template match="PmtInf">
        <PmtInf>
            <PmtInfId>
                <xsl:value-of select="paymentinformationId"/>
            </PmtInfId>
            <PmtMtd>
                <xsl:value-of select="paymentMethod"/>
            </PmtMtd>

            <!--PmtTpInf-->
            <xsl:if test="requestPaymentType">
                <xsl:call-template name="PaymentTypeInformation19">
                    <xsl:with-param name="svcCd" select="requestPaymentType"/>
                </xsl:call-template>
            </xsl:if>

            <ReqdExctnDt>
                <xsl:value-of select="requestExecDate"/>
            </ReqdExctnDt>

            <Dbtr>
                <xsl:apply-templates select="Dbtr"/>
            </Dbtr>

            <xsl:apply-templates select="DbtrAcct"/>
            <xsl:apply-templates select="DbtrAgt"/>

            <!--generate UltmtDbtr with the same result set of Dbtr only for fastpay-->
            <xsl:if test="'URNS'=$reqPaymentType">
                <UltmtDbtr>
                    <xsl:apply-templates select="Dbtr"/>
                </UltmtDbtr>
            </xsl:if>


            <xsl:apply-templates select="ChrgBr"/>

            <xsl:apply-templates select="CdtTrfTxInf"/>
        </PmtInf>
    </xsl:template>

    <xsl:template match="Dbtr">
        <xsl:call-template name="PartyIdentification32">
            <xsl:with-param name="nm" select="debtorName"/>

            <xsl:with-param name="strtNm" select="streetName"/>
            <xsl:with-param name="bldgNb" select="buildingNumber"/>
            <xsl:with-param name="pstCd" select="postCode"/>
            <xsl:with-param name="suburb" select="suburb"/>
            <xsl:with-param name="district" select="district"/>
            <xsl:with-param name="ctry" select="debtorCountry"/>

            <xsl:with-param name="bicorbei" select="debtorBicCode"/>
            <xsl:with-param name="othrid" select="userNumber"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="DbtrAcct">
        <DbtrAcct>
            <xsl:call-template name="CashAccount16">
                <xsl:with-param name="iban" select="debtorIban"/>
                <xsl:with-param name="othrid" select="debtorAccountNumber"/>
                <xsl:with-param name="ccy" select="debtorAccountCurrency"/>
            </xsl:call-template>
        </DbtrAcct>
    </xsl:template>

    <xsl:template match="DbtrAgt">
        <DbtrAgt>
            <xsl:call-template name="BranchAndFinancialInstitutionIdentification4">
                <xsl:with-param name="bic" select="debtorBankBicCode"/>
                <xsl:with-param name="cd" select="debtorClearningCode"/>
                <xsl:with-param name="mmbid" select="debtorInstitutionBsb"/>
                <xsl:with-param name="ctry" select="debtorAgentCountry"/>
            </xsl:call-template>
        </DbtrAgt>
    </xsl:template>

    <xsl:template match="ChrgBr">

        <xsl:if test="chargeBearer">
            <xsl:call-template name="ChargeBearerType1Code">
                <xsl:with-param name="chrgbr" select="chargeBearer"/>
            </xsl:call-template>
        </xsl:if>

        <xsl:if test="creditTrfChangeBearer">
            <xsl:call-template name="ChargeBearerType1Code">
                <xsl:with-param name="chrgbr" select="creditTrfChangeBearer"/>
            </xsl:call-template>
        </xsl:if>

    </xsl:template>


    <xsl:template match="CdtTrfTxInf">
        <CdtTrfTxInf>
            <PmtId>
                <EndToEndId>
                    <xsl:value-of select="paymentRequestId"/>
                </EndToEndId>
            </PmtId>

            <!--PmtTpInf-->
            <xsl:if test="paymentType or salaryPaymentCode">
                <xsl:call-template name="PaymentTypeInformation19">
                    <xsl:with-param name="svcCd" select="paymentType"/>
                    <xsl:with-param name="ctgyCd" select="salaryPaymentCode"/>
                </xsl:call-template>
            </xsl:if>

            <xsl:apply-templates select="Amt"/>

            <xsl:apply-templates select="ChrgBr"/>

            <xsl:apply-templates select="CdtrAgt"/>

            <xsl:apply-templates select="Cdtr"/>

            <xsl:apply-templates select="CdtrAcct"/>

            <!--generate the UltmtCdtr with the same address details of CdtrAgt only for fastpay-->
            <xsl:if test="'URNS'=$reqPaymentType">
                <xsl:if test="$cdtrAgtNm or $cdtrAgtStrtNm or $cdtrAgtbldgNb or $cdtrAgtPstCd or $cdtrAgtSuburb or $cdtrAgtDistrict or $cdtrAgtCtry">
                    <UltmtCdtr>
                        <xsl:call-template name="PartyIdentification32">
                            <xsl:with-param name="nm" select="$cdtrAgtNm"/>
                            <xsl:with-param name="strtNm" select="$cdtrAgtStrtNm"/>
                            <xsl:with-param name="bldgNb" select="$cdtrAgtbldgNb"/>
                            <xsl:with-param name="pstCd" select="$cdtrAgtPstCd"/>
                            <xsl:with-param name="suburb" select="$cdtrAgtSuburb"/>
                            <xsl:with-param name="district" select="$cdtrAgtDistrict"/>
                            <xsl:with-param name="ctry" select="$cdtrAgtCtry"/>
                        </xsl:call-template>
                    </UltmtCdtr>
                </xsl:if>
            </xsl:if>

            <xsl:apply-templates select="RmtInf"/>

        </CdtTrfTxInf>
    </xsl:template>

    <xsl:template match="Amt">
        <Amt>
            <xsl:if test="instructedAmount">
                <InstdAmt>
                    <xsl:attribute name="Ccy">GBP</xsl:attribute>
                    <xsl:value-of select="instructedAmount"/>
                </InstdAmt>
            </xsl:if>
        </Amt>
    </xsl:template>

    <xsl:template match="CdtrAgt">
        <xsl:if test="creditorBankBicCode or creditorClearingCode or creditorInstitutionBsb or creditorBankName or creditorBankCountry">
            <CdtrAgt>

                <xsl:call-template name="BranchAndFinancialInstitutionIdentification4">
                    <xsl:with-param name="bic" select="creditorBankBicCode"/>
                    <xsl:with-param name="cd" select="creditorClearingCode"/>
                    <xsl:with-param name="mmbid" select="creditorInstitutionBsb"/>

                    <xsl:with-param name="nm" select="creditorBankName"/>

                    <xsl:with-param name="strtNm" select="streetName"/>
                    <xsl:with-param name="bldgNb" select="buildingNumber"/>
                    <xsl:with-param name="pstCd" select="postCode"/>
                    <xsl:with-param name="suburb" select="suburb"/>
                    <xsl:with-param name="district" select="district"/>
                    <xsl:with-param name="ctry" select="creditorBankCountry"/>
                </xsl:call-template>

            </CdtrAgt>
        </xsl:if>
    </xsl:template>

    <xsl:template match="Cdtr">
        <xsl:if test="creditorName or $cdtrAgtStrtNm or $cdtrAgtbldgNb or $cdtrAgtPstCd or $cdtrAgtSuburb or $cdtrAgtDistrict or $cdtrAgtCtry">
            <Cdtr>
                <xsl:call-template name="PartyIdentification32">
                    <xsl:with-param name="nm" select="creditorName"/>
                    <xsl:with-param name="strtNm" select="$cdtrAgtStrtNm"/>
                    <xsl:with-param name="bldgNb" select="$cdtrAgtbldgNb"/>
                    <xsl:with-param name="pstCd" select="$cdtrAgtPstCd"/>
                    <xsl:with-param name="suburb" select="$cdtrAgtSuburb"/>
                    <xsl:with-param name="district" select="$cdtrAgtDistrict"/>
                    <xsl:with-param name="ctry" select="$cdtrAgtCtry"/>
                </xsl:call-template>
            </Cdtr>
        </xsl:if>
    </xsl:template>

    <xsl:template match="CdtrAcct">
        <xsl:if test="creditorIban or creditorAccountNumber or creditorAccountName">
            <CdtrAcct>
                <xsl:call-template name="CashAccount16">
                    <xsl:with-param name="iban" select="creditorIban"/>
                    <xsl:with-param name="othrid" select="creditorAccountNumber"/>
                    <xsl:with-param name="nm" select="creditorAccountName"/>
                </xsl:call-template>
            </CdtrAcct>
        </xsl:if>
    </xsl:template>

    <xsl:template match="RmtInf">
        <xsl:if test="paymentNarrative">
            <RmtInf>
                <Ustrd>
                    <xsl:value-of select="paymentNarrative"/>
                </Ustrd>
            </RmtInf>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PartyIdentification32">
        <xsl:param name="nm"/>
        <!--for PostalAddress6-->
        <xsl:param name="strtNm"/>
        <xsl:param name="bldgNb"/>
        <xsl:param name="pstCd"/>
        <xsl:param name="suburb"/>
        <xsl:param name="district"/>
        <xsl:param name="ctry"/>
        <!--for Party6Choice-->
        <xsl:param name="bicorbei"/>
        <xsl:param name="othrid"/>

        <xsl:if test="$nm">
            <Nm>
                <xsl:value-of select="$nm"/>
            </Nm>
        </xsl:if>

        <xsl:if test="$strtNm or $bldgNb or $pstCd or $suburb or $district or $ctry">
           <xsl:call-template name="PostalAddress6">
               <xsl:with-param name="strtNm" select="$strtNm"/>
               <xsl:with-param name="bldgNb" select="$bldgNb"/>
               <xsl:with-param name="pstCd" select="$pstCd"/>
               <xsl:with-param name="suburb" select="$suburb"/>
               <xsl:with-param name="district" select="$district"/>
               <xsl:with-param name="ctry" select="$ctry"/>
           </xsl:call-template>
        </xsl:if>

        <xsl:if test="$bicorbei or $othrid">
           <xsl:call-template name="Party6Choice">
               <xsl:with-param name="bicorbei" select="$bicorbei"/>
               <xsl:with-param name="othrid" select="$othrid"/>
           </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PostalAddress6">
        <xsl:param name="strtNm"/>
        <xsl:param name="bldgNb"/>
        <xsl:param name="pstCd"/>
        <xsl:param name="suburb"/>
        <xsl:param name="district"/>
        <xsl:param name="ctry"/>
        <PstlAdr>
            <xsl:if test="$strtNm">
                <StrtNm>
                    <xsl:value-of select="$strtNm"/>
                </StrtNm>
            </xsl:if>
            <xsl:if test="$bldgNb">
                <BldgNb>
                    <xsl:value-of select="$bldgNb"/>
                </BldgNb>
            </xsl:if>
            <xsl:if test="$pstCd">
                <PstCd>
                    <xsl:value-of select="$pstCd"/>
                </PstCd>
            </xsl:if>
            <xsl:if test="$suburb">
                <TwnNm>
                    <xsl:value-of select="$suburb"/>
                </TwnNm>
            </xsl:if>
            <xsl:if test="$district">
                <CtrySubDvsn>
                    <xsl:value-of select="$district"/>
                </CtrySubDvsn>
            </xsl:if>
            <xsl:if test="$ctry">
                <Ctry>
                    <xsl:value-of select="$ctry"/>
                </Ctry>
            </xsl:if>
        </PstlAdr>
    </xsl:template>

    <xsl:template name="Party6Choice">
        <xsl:param name="bicorbei"/>
        <xsl:param name="othrid"/>
        <Id>
           <OrgId>
               <xsl:if test="$bicorbei">
                   <BICOrBEI>
                       <xsl:value-of select="$bicorbei"/>
                   </BICOrBEI>
               </xsl:if>
               <xsl:if test="$othrid">
                   <Othr>
                       <Id>
                           <xsl:value-of select="$othrid"/>
                       </Id>
                   </Othr>
               </xsl:if>
           </OrgId>
        </Id>
    </xsl:template>

    <xsl:template name="ChargeBearerType1Code">
        <xsl:param name="chrgbr"/>
        <xsl:if test="$chrgbr">
            <ChrgBr>
                <xsl:value-of select="$chrgbr"/>
            </ChrgBr>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PaymentTypeInformation19">
        <xsl:param name="svcCd"/>
        <xsl:param name="ctgyCd"/>

        <PmtTpInf>
            <xsl:if test="$svcCd">
                <SvcLvl>
                    <Cd>
                        <xsl:value-of select="$svcCd"/>
                    </Cd>
                </SvcLvl>
            </xsl:if>

            <xsl:if test="$ctgyCd">
                <CtgyPurp>
                    <Cd>
                        <xsl:value-of select="$ctgyCd"/>
                    </Cd>
                </CtgyPurp>
            </xsl:if>
        </PmtTpInf>

    </xsl:template>

    <xsl:template name="CashAccount16">
        <xsl:param name="iban"/>
        <xsl:param name="othrid"/>
        <xsl:param name="ccy"/>
        <xsl:param name="nm"/>
        <Id>
            <xsl:if test="$iban">
                <IBAN>
                    <xsl:value-of select="$iban"/>
                </IBAN>
            </xsl:if>
            <xsl:if test="$othrid">
                <Othr>
                    <Id>
                        <xsl:value-of select="$othrid"/>
                    </Id>
                </Othr>
            </xsl:if>
        </Id>
        <xsl:if test="$ccy">
            <Ccy>
                <xsl:value-of select="$ccy"/>
            </Ccy>
        </xsl:if>
        <xsl:if test="$nm">
            <Nm>
                <xsl:value-of select="$nm"/>
            </Nm>
        </xsl:if>
    </xsl:template>

    <xsl:template name="BranchAndFinancialInstitutionIdentification4">
        <xsl:param name="bic"/>
        <xsl:param name="cd"/>
        <xsl:param name="mmbid"/>

        <xsl:param name="nm"/>

        <xsl:param name="strtNm"/>
        <xsl:param name="bldgNb"/>
        <xsl:param name="pstCd"/>
        <xsl:param name="suburb"/>
        <xsl:param name="district"/>
        <xsl:param name="ctry"/>

        <FinInstnId>
            <xsl:if test="$bic">
                <BIC>
                    <xsl:value-of select="$bic"/>
                </BIC>
            </xsl:if>

            <xsl:if test="$cd or $mmbid">
                <ClrSysMmbId>
                    <xsl:if test="$cd">
                        <ClrSysId>
                            <Cd>
                                <xsl:value-of select="$cd"/>
                            </Cd>
                        </ClrSysId>
                    </xsl:if>

                    <MmbId>
                        <xsl:value-of select="$mmbid"/>
                    </MmbId>

                </ClrSysMmbId>
            </xsl:if>

            <xsl:if test="$nm">
                <Nm>
                    <xsl:value-of select="$nm"/>
                </Nm>
            </xsl:if>

            <xsl:if test="$strtNm or $bldgNb or $pstCd or $suburb or $district or $ctry">
                <xsl:call-template name="PostalAddress6">
                    <xsl:with-param name="strtNm" select="$strtNm"/>
                    <xsl:with-param name="bldgNb" select="$bldgNb"/>
                    <xsl:with-param name="pstCd" select="$pstCd"/>
                    <xsl:with-param name="suburb" select="$suburb"/>
                    <xsl:with-param name="district" select="$district"/>
                    <xsl:with-param name="ctry" select="$ctry"/>
                </xsl:call-template>
            </xsl:if>
        </FinInstnId>
    </xsl:template>


</xsl:stylesheet>