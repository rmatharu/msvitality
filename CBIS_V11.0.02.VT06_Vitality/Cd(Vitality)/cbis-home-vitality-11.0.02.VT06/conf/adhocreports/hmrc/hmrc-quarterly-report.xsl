<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns="http://www.govtalk.gov.uk/taxation/PSOnline/AccountingForTax/5">

    <xsl:output method="xml" indent="yes"/>

    <xsl:template match="hmrc">
        <AccountingForTax>
            <VersionNumber>5</VersionNumber>
            <SchemeDetails>
                <xsl:apply-templates select="SchemeDetails"/>
            </SchemeDetails>
            <TaxDeducted>
                <xsl:apply-templates select="TaxDeducted"/>
            </TaxDeducted>
            <xsl:copy-of select="TaxDeducted/AmendedReturn"/>

            <xsl:apply-templates select="ShortServiceRefund"/>

            <xsl:apply-templates select="SpecialDeathBenefits"/>

            <xsl:call-template name="writeAuthorisedSurplus"/>
            <xsl:call-template name="writeLifetimeAllowance"/>
            <xsl:call-template name="writeAnnualAllowanceChargeDetails"/>
            <xsl:call-template name="writeTotalTax"/>

            <xsl:apply-templates select="Declaration"/>
        </AccountingForTax>
    </xsl:template>

    <!--  
        | process SchemeDetails: this is a compulsory element with two elements which are among two
        | choosen groups, if all of them are empty, just give <TaxReference/> and <SchemeName/>
        -->
    <xsl:template match="SchemeDetails">
        <xsl:choose>
            <xsl:when test="boolean(TaxReference[string-length(text())>0])">
                <xsl:copy-of select="TaxReference"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- in case it's missing, just give a empty TaxReference, cause this is compulsory element -->
                <TaxReference> </TaxReference>
            </xsl:otherwise>
        </xsl:choose>
        <xsl:choose>
            <xsl:when test="boolean(SchemeName[string-length(text())>0])">
                <xsl:copy-of select="SchemeName"/>
            </xsl:when>
            <xsl:otherwise>
                <!-- in case it's missing, just give a empty SchemeName, cause this is compulsory element -->
                <SchemeName> </SchemeName>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--  
        | process TaxDeducted: this is a compulsory element, simply copy all tags
        -->
    <xsl:template match="TaxDeducted">
        <xsl:copy-of select="Quarter"/>
        <xsl:copy-of select="Year"/>
    </xsl:template>

    <!--  
        | process ShortServiceRefund: this is a optional tags, 
        | but all its children nodes are compulsory element, simply copy all tags
        -->
    <xsl:template match="ShortServiceRefund">
        <xsl:if test="boolean(*)">
            <ShortServiceRefund>
                <xsl:call-template name="copyAll"/>
            </ShortServiceRefund>
        </xsl:if>
    </xsl:template>

    <!--  
        | process SpecialDeathBenefits: this is a optional tags, 
        | but Member and TotalTax nodes are compulsory element, simply copy their value regardless 
        | their values are empty or not
        -->
    <xsl:template match="SpecialDeathBenefits">
        <xsl:if test="boolean(*)">
            <SpecialDeathBenefits>
                <Members>
                    <xsl:value-of select="members"/>
                </Members>
                <TotalTax>
                    <xsl:value-of select="Totaltax"/>
                </TotalTax>
            </SpecialDeathBenefits>
        </xsl:if>
    </xsl:template>

    <xsl:template name="writeAuthorisedSurplus">
        <xsl:if test="count(EmployerDetails)>0">
            <!--  30-Oct-2015  sp_rpt_hmrc_quarterly_ret is hardwired to return an empty result for EmployerDetails -->
            <AuthorisedSurplus>
                <xsl:copy-of select="Employers"/>
                <xsl:copy-of select="TotalTax"/>
                <xsl:apply-templates select="EmployerDetails"/>
            </AuthorisedSurplus>
        </xsl:if>
    </xsl:template>

    <!--  
        | process LifetimeAllowance: this is a optional tags, 
        | but Member, TotalTax and Member nodes are compulsory element, simply copy their value regardless 
        | their values are empty or not. for processing Member tags call MemberDetails template
        -->
    <xsl:template name="writeLifetimeAllowance">
        <xsl:if test="count(MemberDetails)>0">
            <LifetimeAllowance>
                <Members>
                    <xsl:value-of
                        select="MemberDetails[1]/members"/>
                </Members>
                <TotalTax>
                    <xsl:value-of
                        select="MemberDetails[1]/TotalTax"/>
                </TotalTax>
                <xsl:apply-templates select="MemberDetails"/>
            </LifetimeAllowance>
        </xsl:if>
    </xsl:template>
    
    
       <xsl:template name="writeAnnualAllowanceChargeDetails">
        <xsl:if test="count(AnnualAllowanceChargeDetails)>0">
            <AnnualAllowance>
                <Members>
                    <xsl:value-of
                        select="AnnualAllowanceChargeDetails[1]/members"/>
                </Members>
                <TotalChargeAmount>
                    <xsl:value-of
                        select="AnnualAllowanceChargeDetails[1]/TotalChargeAmount"/>
                </TotalChargeAmount>
                <xsl:apply-templates select="AnnualAllowanceChargeDetails"/>
            </AnnualAllowance>
        </xsl:if>
    </xsl:template>
    
    

    <!--  
        | process total tax: sum of the SpecialDeathBenefitTotalTax and LifetimeAllowanceTotalTax and AnnualAllowanceChargeDetails[1]/TotalChargeAmount
        | if one of them is empty then sum will be the left one; 
        | if all of them are empty then this section will be empty
        | does not like checking in the following format for some reason... test="boolean(//SpecialDeathBenefits/TotalTax[number(text())>0]) or boolean(//MemberDetails[1]/TotalTax[number(text())>0])">
        -->
    <xsl:template name="writeTotalTax">
        <xsl:variable name="SpecialDeathBenefitTotalTax">
            <xsl:choose>
                <xsl:when test="string(number(SpecialDeathBenefits/Totaltax)) = 'NaN'">0</xsl:when>
                <xsl:otherwise><xsl:value-of select="number(SpecialDeathBenefits/Totaltax)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="LifetimeAllowanceTotalTax">
            <xsl:choose>
                <xsl:when test="string(number(MemberDetails[1]/TotalTax)) = 'NaN'">0</xsl:when>
                <xsl:otherwise><xsl:value-of select="number(MemberDetails[1]/TotalTax)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="AnnualAllowanceTotalTax">
            <xsl:choose>
                <xsl:when test="string(number(AnnualAllowanceChargeDetails[1]/TotalChargeAmount)) = 'NaN'">0</xsl:when>
                <xsl:otherwise><xsl:value-of select="number(AnnualAllowanceChargeDetails[1]/TotalChargeAmount)"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="($SpecialDeathBenefitTotalTax + $LifetimeAllowanceTotalTax + $AnnualAllowanceTotalTax) > 0">
                <TotalTax><xsl:value-of select='format-number($SpecialDeathBenefitTotalTax + $LifetimeAllowanceTotalTax + $AnnualAllowanceTotalTax, "#.00")'/></TotalTax>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <!--  
        | process EmployerDetails tag
        | need testing (no data in GEN_COPY databse)
        -->
    <xsl:template match="EmployerDetails">
        <Employer>
            <xsl:choose>
                <xsl:when test="not(organization)">
                    <Individual>
                        <xsl:if test="boolean(title[string-length(text())>0])">
                            <Ttl>
                                <xsl:value-of select="title"/>
                            </Ttl>
                        </xsl:if>
                        <xsl:if test="boolean(forename[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(forename2[string-length(text())>0])">
                            <Fore>
                                <xsl:value-of select="forename2"/>
                            </Fore>
                        </xsl:if>
                        <xsl:if test="boolean(surname[string-length(text())>0])">
                            <Sur>
                                <xsl:value-of select="surname"/>
                            </Sur>
                        </xsl:if>
                    </Individual>
                </xsl:when>
                <xsl:otherwise>
                    <Organization>
                        <xsl:value-of select="organization"/>
                    </Organization>
                </xsl:otherwise>
            </xsl:choose>
            <CRN>
                <xsl:value-of select="crn"/>
            </CRN>
            <Address>
                <xsl:call-template name="processCommonFields">
                    <xsl:with-param name="fieldStr" select="'addr'"/>
                    <xsl:with-param name="newTag" select="'Line'"/>
                </xsl:call-template>
                <xsl:if test="boolean(postcode[string-length(text())>0])">
                    <PostCode>
                        <xsl:value-of select="postcode"/>
                    </PostCode>
                </xsl:if>
                <Country>
                    <xsl:value-of select="country"/>
                </Country>
            </Address>
            <Payment>
                <Date>
                    <xsl:value-of select="payment_date"/>
                </Date>
                <Tax>
                    <xsl:value-of select="payment_tax"/>
                </Tax>
            </Payment>
        </Employer>
    </xsl:template>

    <!--  
        | process MemberDetails tag
        -->
    <xsl:template match="MemberDetails">
        <Member>
            <xsl:if test="boolean(pstr[string-length(text())>0])">
                <Pstr><xsl:value-of select="pstr"/></Pstr>
                <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
            </xsl:if>
            <Name>
                <xsl:if test="boolean(title[string-length(text())>0])">
                    <Ttl>
                        <xsl:value-of select="title"/>
                    </Ttl>
                </xsl:if>
                <xsl:if test="boolean(forename[string-length(text())>0])">
                    <Fore>
                        <xsl:value-of select="forename"/>
                    </Fore>
                </xsl:if>
                <xsl:if test="boolean(forename2[string-length(text())>0])">
                    <Fore>
                        <xsl:value-of select="forename2"/>
                    </Fore>
                </xsl:if>
                <xsl:if test="boolean(surname[string-length(text())>0])">
                    <Sur>
                        <xsl:value-of select="surname"/>
                    </Sur>
                </xsl:if>
            </Name>
            <xsl:choose>
                <xsl:when test="boolean(tax_file_number[string-length(text())>0])">
                    <NINO>
                        <xsl:value-of select="tax_file_number"/>
                    </NINO>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="boolean(birth_date[string-length(text())>0])">
                        <DOB>
                            <xsl:value-of select="birth_date"/>
                        </DOB>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
            <Event>
                <BenefitDate>
                    <xsl:value-of select="crystallisation_date"/>
                </BenefitDate>
                <xsl:if test="boolean(LAC25Percent[string-length(text())>0])">
                    <xsl:copy-of select="LAC25Percent"/>
                </xsl:if>
                <xsl:if test="boolean(LAC55Percent[string-length(text())>0])">
                    <xsl:copy-of select="LAC55Percent"/>
                </xsl:if>
            </Event>
        </Member>
    </xsl:template>
    
    
    <!--  
		process AnnualAllowanceChargeDetails tag
        -->
    <xsl:template match="AnnualAllowanceChargeDetails">
        <Member>
            <xsl:if test="boolean(pstr[string-length(text())>0])">
                <Pstr><xsl:value-of select="pstr"/></Pstr>
                <MemberAccountId><xsl:value-of select="member_account_id"/></MemberAccountId>
            </xsl:if>
            <Name>
                <xsl:if test="boolean(title[string-length(text())>0])">
                    <Ttl>
                        <xsl:value-of select="title"/>
                    </Ttl>
                </xsl:if>
                <xsl:if test="boolean(forename[string-length(text())>0])">
                    <Fore>
                        <xsl:value-of select="forename"/>
                    </Fore>
                </xsl:if>
                <xsl:if test="boolean(forename2[string-length(text())>0])">
                    <Fore>
                        <xsl:value-of select="forename2"/>
                    </Fore>
                </xsl:if>
                <xsl:if test="boolean(surname[string-length(text())>0])">
                    <Sur>
                        <xsl:value-of select="surname"/>
                    </Sur>
                </xsl:if>
            </Name>
            <xsl:if test="boolean(tax_file_number[string-length(text())>0])">
                <NINO>
                    <xsl:value-of select="tax_file_number"/>
                </NINO>
            </xsl:if>
            <xsl:if test="boolean(date_of_notice[string-length(text())>0])">
                <DateOfNotice>
                    <xsl:value-of select="date_of_notice"/>
                </DateOfNotice>
            </xsl:if>
            <xsl:if test="boolean(ChargeAmount[string-length(text())>0])">
                 <xsl:copy-of select="ChargeAmount"/>
            </xsl:if>
            <xsl:if test="boolean(tax_year[string-length(text())>0])">
                <TaxYear>
                    <xsl:value-of select="tax_year"/>
                </TaxYear>
            </xsl:if>
            
        </Member>
    </xsl:template>
    

    <!--  
        | process Declaration tag
        -->
    <xsl:template match="Declaration">
        <Declaration>
            <Administrator>
                <xsl:choose>
                    <xsl:when test="not(organization)">
                        <Individual>
                            <xsl:if test="boolean(title[string-length(text())>0])">
                                <Ttl>
                                    <xsl:value-of select="title"/>
                                </Ttl>
                            </xsl:if>
                            <xsl:if test="boolean(forename[string-length(text())>0])">
                                <Fore>
                                    <xsl:value-of select="forename"/>
                                </Fore>
                            </xsl:if>
                            <xsl:if test="boolean(forename2[string-length(text())>0])">
                                <Fore>
                                    <xsl:value-of select="forename2"/>
                                </Fore>
                            </xsl:if>
                            <xsl:if test="boolean(surname[string-length(text())>0])">
                                <Sur>
                                    <xsl:value-of select="surname"/>
                                </Sur>
                            </xsl:if>
                        </Individual>
                    </xsl:when>
                    <xsl:otherwise>
                        <Organisation>
                            <xsl:value-of select="organization"/>
                        </Organisation>
                    </xsl:otherwise>
                </xsl:choose>
                <Address>
                    <xsl:call-template name="processCommonFields">
                        <xsl:with-param name="fieldStr" select="'addr'"/>
                        <xsl:with-param name="newTag" select="'Line'"/>
                    </xsl:call-template>
                    <xsl:if test="boolean(postcode[string-length(text())>0])">
                        <PostCode>
                            <xsl:value-of select="postcode"/>
                        </PostCode>
                    </xsl:if>
                    <Country>
                        <xsl:value-of select="country"/>
                    </Country>
                </Address>
                <xsl:if test="boolean(phone_number[string-length(text())>0])">
                    <Telephone>
                        <xsl:value-of select="phone_number"/>
                    </Telephone>
                </xsl:if>
                <xsl:if test="boolean(email_address[string-length(text())>0])">
                    <Email>
                        <xsl:value-of select="child::email_address"/>
                    </Email>
                </xsl:if>
            </Administrator>
            <xsl:choose>
                <xsl:when
                    test="(count(InformationCorrect)>0)
                                and
                                (count(NoFalseStatements)>0)">
                    <xsl:copy-of select="InformationCorrect"/>
                    <xsl:copy-of select="NoFalseStatements"/>
                </xsl:when>
                <xsl:when
                    test="(count(ApprovedByAdministrator)>0)
                                and
                                (count(Authorized)>0)">
                    <xsl:copy-of select="ApprovedByAdministrator"/>
                    <xsl:copy-of select="Authorized"/>
                </xsl:when>
            </xsl:choose>
        </Declaration>
    </xsl:template>

    <!--  
        | process some common tags which start with the same string (eg, address1, address2, ..);
        | input parameters: 
        |     fieldStr: string which is the common part of source tag name shared by all tags (eg. "address")
        |     newTag: new targt tag name  
        -->
    <xsl:template name="processCommonFields">
        <xsl:param name="fieldStr"/>
        <xsl:param name="newTag"/>
        <xsl:for-each select="*[starts-with(name(),$fieldStr)]">
            <xsl:sort select="name()"/>
            <xsl:if test="boolean(current()[string-length(text())>0])">
                <xsl:element name="{$newTag}">
                    <xsl:choose>
                        <xsl:when test="text()='unknown'">
                            <xsl:text> </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:element>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <!--  
        | just copy all children nodes regardless of they are empty or not;
        | this is for the case of all the children nodes are compulsory 
        -->
    <xsl:template name="copyAll">
        <xsl:for-each select="*">
            <xsl:copy-of select="."/>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
