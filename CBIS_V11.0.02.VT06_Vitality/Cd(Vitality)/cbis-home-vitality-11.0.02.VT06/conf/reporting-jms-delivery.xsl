<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm">

    <xsl:template match="/">
        <JMSDeliveryNotify>
            <processType><xsl:value-of select="/cm:CBISNM/cm:properties/cbis-processname"/></processType>
            <processId><xsl:value-of select="/cm:CBISNM/cm:properties/composer.submitted_process_id"/></processId>
            <reportId><xsl:value-of select="/cm:CBISNM/cm:properties/cbis-run-id"/></reportId>
        </JMSDeliveryNotify>            
    </xsl:template>
</xsl:stylesheet>
