<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
    xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
    exclude-result-prefixes="cm" >
<!--
    <xsl:template match="/">
        <xsl:copy-of select="/cm:CBISNM/cm:payload/documentHandling"/>
    </xsl:template>
-->
    <xsl:template match="/">
            <xsl:variable name="cbis-work-dir" select="'D:\InfoComp\cbis\work\reporting\data\approved\'"/>
            <xsl:variable name="cbis-home" select="'D:\InfoComp\cbis-1.0.2\reporting\cbis_home\'"/>
        <CreateDocument xmlns="http://internal.macquarie.com/ws/ISD/IBG/Financial_Products/ComposerLink">
            <processType><xsl:value-of select="/cm:CBISNM/cm:properties/cbis-processname"/></processType>
            <documentPath><xsl:value-of select="translate(concat($cbis-work-dir, /cm:CBISNM/cm:properties/cbis-filenamestub, '.pdf'),'/','\')"/></documentPath>
            <partyTypeID><xsl:value-of select="/cm:CBISNM/cm:properties/cbis-party-type-id"/></partyTypeID>
            <partyID><xsl:value-of select="/cm:CBISNM/cm:properties/cbis-party-id"/></partyID>
            <templateFilename><xsl:value-of select="translate(concat($cbis-home, /cm:CBISNM/cm:properties/cbis-report-template),'/','\')"/></templateFilename>
            <sourceXMLPath><xsl:value-of select="translate(concat($cbis-work-dir, /cm:CBISNM/cm:properties/cbis-filenamestub, '.xml'),'/','\')"/></sourceXMLPath>
        </CreateDocument>            
    </xsl:template>
</xsl:stylesheet>
