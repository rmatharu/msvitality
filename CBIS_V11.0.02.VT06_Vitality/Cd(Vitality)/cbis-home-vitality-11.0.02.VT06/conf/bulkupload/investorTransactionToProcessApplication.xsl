<?xml version="1.0" encoding="UTF-8"?>
<!--
  Transform an investorTransaction encapsulated in a CBISNM into a CreateCorrespondenceRequest
 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common"
    xmlns:jbi="xalan://org.apache.servicemix.components.xslt.XalanExtension"
    xmlns:redirect="http://xml.apache.org/xalan/redirect"
    xmlns:cbis="http://www.infocomp.com/cbis/v1"
    xmlns:cbisnm="http://www.infocomp.com/cbis/v1/cbisnm"
    xmlns:upload="http://www.infocomp.com/conductor/cases/business/upload"
    xmlns:transaction="http://www.infocomp.com/conductor/cases/business/transaction"
    xmlns:investoraccount="http://www.infocomp.com/conductor/cases/business/investoraccount"
    xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
    xmlns:old="http://www.infocomp.com/conductor/cases/business/transaction"
    xmlns:new="http://www.infocomp.com/conductor/cases/business/applications"
    extension-element-prefixes="jbi redirect exsl"
    exclude-result-prefixes="xsl investoraccount new old cm transaction upload cbis"
    version="1.0">

    <!-- investorId and productTypeId are determined from an ICXML
         request from the investorAccountId specified in the account
         declaration or returned from the CreateInvestorRequest

         serverDate and userId are attached to the above ICXML request and passed
         in for later use.
    -->

    <!--
    Uncomment this line when debugging. This will indent the transformed message
    -->
    <xsl:output method="xml" indent="yes" version="1.0"/>

    <xsl:param name="old-ns-prefix" select="'http://www.infocomp.com/conductor/cases/business/transaction'"/>
    <xsl:param name="new-ns-prefix" select="'http://www.infocomp.com/conductor/cases/business/applications'"/>

    <!--
    This is the main template that is used to transform the CBIS Normalised Message XML
    -->
    <xsl:template match="/">
        	<ProcessApplicationRequest
                xmlns="http://www.infocomp.com/conductor/cases/business/applications">
                    <!-- Need to apply the ICProcessRequest template-->
                    <xsl:apply-templates select="//transaction:transactionDetails" />
            </ProcessApplicationRequest>
	</xsl:template>

    <!-- The investorTransaction template - this creates the CCR for us -->
    <xsl:template match="transaction:transactionDetails">
        <trustApplication xmlns="http://www.infocomp.com/conductor/cases/business/applications">
            <correspondenceId>
            	<xsl:choose>
		           	<xsl:when test="../transaction:correspondenceId">
            			<xsl:value-of select="../transaction:correspondenceId"/>
            		</xsl:when>
            		<xsl:otherwise>
            			<xsl:value-of select="/cm:CBISNM/cm:properties/correspondenceId"/>
            		</xsl:otherwise>
           		</xsl:choose>
            </correspondenceId>
            <amount><xsl:value-of select="transaction:application/transaction:amount"/></amount>
            <marketingCodeId><xsl:value-of select="transaction:application/transaction:marketingCodeId"/></marketingCodeId>
            <applicationInvestmentValueType><xsl:value-of select="transaction:application/transaction:investmentValueType"/></applicationInvestmentValueType>
            <xsl:apply-templates select="transaction:application/transaction:transactionInvestment"/>
            <xsl:apply-templates select="transaction:application/transaction:defaultTransactionBrokerage"/>
        </trustApplication>
    </xsl:template>

    <xsl:template match="transaction:transactionInvestment">
        <applicationInvestment xmlns="http://www.infocomp.com/conductor/cases/business/applications">
            <xsl:apply-templates select="child::*" mode="transform"/>
        </applicationInvestment>
    </xsl:template>

    <xsl:template match="transaction:defaultTransactionBrokerage">
        <defaultApplicationBrokerage  xmlns="http://www.infocomp.com/conductor/cases/business/applications">
            <xsl:apply-templates select="child::*" mode="transform"/>
        </defaultApplicationBrokerage>
    </xsl:template>

    <!--
    <xsl:template match="@*|node()" mode="keep">
        <xsl:choose>
            <xsl:when test="namespace-uri(.) = $old-ns-prefix">
                <xsl:element name="{local-name(.)}" namespace="{$new-ns-prefix}">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates select="child::*" mode="keep"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:value-of select="."/>
                    <xsl:apply-templates select="@*|node()" mode="keep"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    -->

    <xsl:template match="*" mode="transform">
        <xsl:choose>
            <xsl:when test="namespace-uri(.) = $old-ns-prefix">
                <xsl:element name="{local-name(.)}" namespace="{$new-ns-prefix}">
                    <xsl:copy-of select="@*"/>
                    <xsl:apply-templates mode="transform"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="." mode="keep"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="node()" mode="keep">
            <xsl:element name="{local-name(.)}" namespace="{namespace-uri(.)}">
                <xsl:value-of select="."/>
                <xsl:copy-of select="@*"/>
              <xsl:apply-templates select="child::*" mode="keep"/>
            </xsl:element>
    </xsl:template>

</xsl:stylesheet>
