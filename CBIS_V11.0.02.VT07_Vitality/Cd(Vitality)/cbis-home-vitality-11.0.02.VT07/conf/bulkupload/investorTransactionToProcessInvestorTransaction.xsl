<?xml version="1.0" encoding="UTF-8"?>
<!--
    Transform an investorTransaction encapsulated in a CBISNM into a CreateCorrespondenceRequest
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:exsl="http://exslt.org/common"
    xmlns:jbi="xalan://org.apache.servicemix.components.xslt.XalanExtension"
    xmlns:redirect="http://xml.apache.org/xalan/redirect"
    xmlns:cbis="http://www.infocomp.com/cbis/v1"
    xmlns:upload="http://www.infocomp.com/conductor/cases/business/upload"
    xmlns:transaction="http://www.infocomp.com/conductor/cases/business/transaction"
    xmlns:cm="http://www.infocomp.com/cbis/v1/cbisnm"
    extension-element-prefixes="jbi redirect exsl"
    version="1.0">

    <!-- investorId and productTypeId are determined from an ICXML
        request from the investorAccountId specified in the account
        declaration or returned from the CreateInvestorRequest

        serverDate and userId are attached to the above ICXML request and passed
        in for later use.
    -->

    <!--
        Uncomment this line when debugging. This will indent the transformed message
        <xsl:output method="xml" indent="yes" version="1.0"/>
    -->

    <!--
        This is the main template that is used to transform the CBIS Normalised Message XML
    -->
    <xsl:template match="/">
        <!--TODO: Need to represent the correct service to be used within servicemix. -->
        <!-- this wraps the payload with the Conductor message name - needed when dealing with messages defined in WSDLs -->
        <!-- the Transformed XML Message needs to contain all the properties since we don't need to look at these for path remapping.-->
        <!-- We need to transform the payload section of the CBIS Normalised Message since this is where the paths to be remapped will be located.-->
            <xsl:apply-templates select="//upload:investorTransaction|//investorTransaction"/>
    </xsl:template>

    <!-- The investorTransaction template - this creates the CCR for us -->
    <xsl:template match="upload:investorTransaction|investorTransaction">
            <ProcessInvestorTransactionRequest xmlns="http://www.infocomp.com/conductor/cases/business/transaction">
                <!-- the transactionReference here is translated into the to_from_flag
                    which for redemptions needs to be F
                -->
                <investorTransactionDetails>
                    <xsl:choose>
                        <xsl:when test="/cm:CBISNM/cm:properties/investorAccountId">
                            <accountDeclaration investorAccountId="/cm:CBISNM/cm:properties/investorAccountId"/>
                        </xsl:when>
                    </xsl:choose>
                    <xsl:copy-of select="transaction:correspondenceDetails"/>
                    <xsl:apply-templates select="transaction:transactionDetails"/>
                </investorTransactionDetails>
            </ProcessInvestorTransactionRequest>
    </xsl:template>

    <xsl:template match="transaction:transactionDetails">
        <transactionDetails transactionReference="FROM"
            xmlns="http://www.infocomp.com/conductor/cases/business/transaction">
            <correspondenceId>
                <xsl:choose>
                    <xsl:when test="../transaction:correspondenceId">
                        <xsl:value-of select="../transaction:correspondenceId"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="/cm:CBISNM/cm:properties/correspondenceId"/>
                    </xsl:otherwise>
                </xsl:choose>
            </correspondenceId>
            <xsl:copy-of select="*/*"/>
        </transactionDetails>
    </xsl:template>

</xsl:stylesheet>
