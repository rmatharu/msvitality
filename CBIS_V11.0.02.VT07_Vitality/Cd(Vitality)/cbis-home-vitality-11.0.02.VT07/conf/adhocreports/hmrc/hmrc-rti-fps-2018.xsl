<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns="http://www.govtalk.gov.uk/CM/envelope" >
    <xsl:output method="xml" indent="yes" />
    <xsl:template match="hmrc">
        <!-- <xsl:copy-of select="."/>  IDEM transformer -->
        <GovTalkMessage>
            <EnvelopeVersion>2.0</EnvelopeVersion>
            <xsl:apply-templates select="EasConstants"/>   <!-- these items are once per entire message -->
            <Body>
                <IRenvelope xmlns="http://www.govtalk.gov.uk/taxation/PAYE/RTI/FullPaymentSubmission/17-18/1">
                    <IRheader>
                        <Keys>
                            <Key Type="TaxOfficeNumber"><xsl:value-of select="EasConstants/tax_office_code"/></Key>
                            <Key Type="TaxOfficeReference"><xsl:value-of select="EasConstants/paye_reference"/></Key>
                        </Keys>
                        <PeriodEnd>
                            <xsl:value-of select="substring(EasConstants/period_end,0,11)"/>
                        </PeriodEnd>
                        <DefaultCurrency>GBP</DefaultCurrency>
                        <IRmark Type="generic">ZbJSKtKSjhAgKt9/c5i3GbiT0G4=</IRmark>
                        <Sender>Employer</Sender>
                    </IRheader>
                    <FullPaymentSubmission>
                        <EmpRefs>
                            <OfficeNo><xsl:value-of select="EasConstants/tax_office_code"/></OfficeNo>
                            <PayeRef><xsl:value-of select="EasConstants/paye_reference"/></PayeRef>
                            <AORef><xsl:value-of select="EasConstants/emp_accts_office_ref"/></AORef>
                        </EmpRefs>
                        <RelatedTaxYear><xsl:value-of select="EasConstants/tax_year"/></RelatedTaxYear>

                        <xsl:for-each select="EmployeeRecord">
                            <xsl:call-template name="OneEmployee"></xsl:call-template>
                        </xsl:for-each>

                        <xsl:call-template name="QuestionsDeclarations">
                            <!-- our QuestionsDeclarations template also includes the FinalSubmission -->
                        </xsl:call-template>

                    </FullPaymentSubmission>
                </IRenvelope>

            </Body>

        </GovTalkMessage>
    </xsl:template>

    <xsl:template match="EasConstants" >
        <xsl:element name="Header" >
            <MessageDetails>
                <xsl:if test="test_submission = 1">
                    <Class>HMRC-PAYE-RTI-FPS-TIL</Class>
                </xsl:if>
                <xsl:if test="test_submission != 1">
                    <Class>HMRC-PAYE-RTI-FPS</Class>
                </xsl:if>
                <Qualifier>request</Qualifier>
                <Function>submit</Function>
                <TransactionID><xsl:value-of select="batch_run_id"/></TransactionID>
                <!--  the CorrelationID must be in the right place, with a child of an empty Text node -->
                <CorrelationID></CorrelationID>
                <Transformation>XML</Transformation>
                <!--  set GatewayTest to a constant, which will be replaced later, depending on the URL configured in CBIS -->
                <GatewayTest>2</GatewayTest>
            </MessageDetails>
            <SenderDetails>
                <IDAuthentication>
                    <SenderID><xsl:value-of select="sender_id"/></SenderID>
                    <Authentication>
                        <Method>clear</Method>
                        <Role>principal</Role>
                        <Value><xsl:value-of select="authentication"/></Value>
                    </Authentication>
                </IDAuthentication>
            </SenderDetails>
        </xsl:element>
        <GovTalkDetails>
            <Keys>
                <Key Type="TaxOfficeNumber"><xsl:value-of select="tax_office_code"/></Key>
                <Key Type="TaxOfficeReference"><xsl:value-of select="paye_reference"/></Key>
            </Keys>
            <ChannelRouting>
                <Channel>
                    <URI>1800</URI>  <!-- vendor ID can be hard wired -->
                    <Product><xsl:value-of select="product_name"/></Product>
                    <Version><xsl:value-of select="product_version"/></Version>
                </Channel>
                <Timestamp>
                    <xsl:value-of select="concat( substring(time_right_now,0,11), 'T',substring(time_right_now,12,12))"/>
                </Timestamp>
            </ChannelRouting>
        </GovTalkDetails>
    </xsl:template>

    <xsl:template name="QuestionsDeclarations" >

        <xsl:if test="boolean(QuestionsDeclarations/final_submission_scheme_ceased[string-length(text())>0])
            or boolean(QuestionsDeclarations/final_submission              [string-length(text())>0])">
            <FinalSubmission xmlns="http://www.govtalk.gov.uk/taxation/PAYE/RTI/FullPaymentSubmission/17-18/1">
                <!-- if we repeat the same xmlns as the parent, then the transformer does not show it on the output -->
                <!-- but if we specify no xmlns, then the transformer inserts the name space of the top element, which is wrong -->
                <xsl:if test="boolean(QuestionsDeclarations/final_submission_scheme_ceased[string-length(text())>0])">
                    <BecauseSchemeCeased> <!-- this element implies the presence of the date -->
                        <xsl:value-of select="QuestionsDeclarations/final_submission_scheme_ceased"/>
                    </BecauseSchemeCeased>
                    <DateSchemeCeased>
                        <xsl:value-of select="substring(QuestionsDeclarations/date_scheme_ceased,0,11)"/>
                    </DateSchemeCeased>
                </xsl:if>
                <xsl:if test="boolean(QuestionsDeclarations/final_submission[string-length(text())>0])">
                    <ForYear>
                        <xsl:value-of select="QuestionsDeclarations/final_submission"/>
                    </ForYear>
                </xsl:if>
            </FinalSubmission>
            <!-- no more  QuestionsDeclarations section in generated XML for year 2017 -->
            <!-- we just use the name QuestionsDeclarations to indicate the third result set -->
        </xsl:if>
    </xsl:template>

    <xsl:template name="OneEmployee">
        <Employee xmlns="http://www.govtalk.gov.uk/taxation/PAYE/RTI/FullPaymentSubmission/17-18/1">
            <EmployeeDetails>
                <xsl:if test="boolean(tax_file_number[string-length(text())>0])">
                    <NINO>
                        <xsl:value-of select="tax_file_number"/>
                    </NINO>
                </xsl:if>
                <Name>
                    <xsl:if test="boolean(title_name[string-length(text())>0])">
                        <Ttl>
                            <xsl:value-of select="title_name"/>
                        </Ttl>
                    </xsl:if>
                    <xsl:if test="boolean(given_name[string-length(text())>0])">
                        <Fore>
                            <xsl:value-of select="given_name"/>
                        </Fore>
                    </xsl:if>
                    <xsl:if test="boolean(second_name[string-length(text())>0])">
                        <Fore>
                            <xsl:value-of select="second_name"/>
                        </Fore>
                    </xsl:if>
                    <xsl:if test="boolean(initial[string-length(text())>0])">
                        <Initials>
                            <xsl:value-of select="initial"/>
                        </Initials>
                    </xsl:if>
                    <xsl:if test="boolean(surname[string-length(text())>0])">
                        <Sur>
                            <xsl:value-of select="surname"/>
                        </Sur>
                    </xsl:if>
                </Name>
                <Address>
                    <xsl:if test="boolean(address_line_1[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_1"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(address_line_2[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_2"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(address_line_3[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_3"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(address_line_4[string-length(text())>0])">
                        <Line>
                            <xsl:call-template name="CheckLeadingCharacter">
                                <xsl:with-param name="addressline">
                                    <xsl:value-of select="address_line_4"/>
                                </xsl:with-param>
                            </xsl:call-template>
                        </Line>
                    </xsl:if>
                    <xsl:if test="boolean(postcode[string-length(text())>0])">
                        <UKPostcode>
                            <xsl:value-of select="postcode"/>
                        </UKPostcode>
                    </xsl:if>
                    <xsl:if test="boolean(countrycode[string-length(text())>0])">
                        <ForeignCountry>
                            <xsl:value-of select="countrycode"/>
                        </ForeignCountry>
                    </xsl:if>
                </Address>
                <xsl:if test="boolean(birth_date[string-length(text())>0])">
                    <BirthDate>
                        <xsl:value-of select="substring(birth_date,0,11)"/>
                    </BirthDate>
                </xsl:if>
                <Gender><xsl:value-of select="gender"/></Gender>
            </EmployeeDetails>
            <Employment>
                <xsl:if test="boolean(occ_pen_flag[string-length(text())>0])">
                    <OccPenInd>
                        <xsl:value-of select="occ_pen_flag"/>
                    </OccPenInd>
                </xsl:if>
                <xsl:if test="boolean(start_date           [string-length(text())>0])
                    or boolean(student_loan_flag    [string-length(text())>0])
                    or boolean(pension_annual_amount[string-length(text())>0])">
                    <Starter>
                        <xsl:if test="boolean(start_date[string-length(text())>0])">
                            <StartDate><xsl:value-of select="substring(start_date,0,11)"/></StartDate>
                        </xsl:if>
                        <xsl:if test="boolean(student_loan_flag[string-length(text())>0])">
                            <StudentLoan><xsl:value-of select="student_loan_flag"/></StudentLoan>
                        </xsl:if>
                        <xsl:if test="boolean(pension_annual_amount[string-length(text())>0])">
                            <OccPension>
                                <xsl:if test="boolean(bereaved_flag[string-length(text())>0])">
                                    <Bereaved><xsl:value-of select="bereaved_flag"/></Bereaved>
                                </xsl:if>
                                <Amount><xsl:value-of select="pension_annual_amount"/></Amount>
                            </OccPension>
                        </xsl:if>
                    </Starter>
                </xsl:if>
                <xsl:if test="boolean(hmrc_payroll_id[string-length(text())>0])">
                    <PayId><xsl:value-of select="hmrc_payroll_id"/></PayId>
                </xsl:if>

                <xsl:if test="boolean(payroll_id_changed_flag[string-length(text())>0])
                    or boolean(old_hmrc_payroll_id    [string-length(text())>0])">
                    <PayIdChgd>
                        <xsl:if test="boolean(payroll_id_changed_flag[string-length(text())>0])">
                            <PayrollIdChangedIndicator>
                                <xsl:value-of select="payroll_id_changed_flag"/>
                            </PayrollIdChangedIndicator>
                        </xsl:if>
                        <xsl:if test="boolean(old_hmrc_payroll_id[string-length(text())>0])">
                            <OldPayrollId>
                                <xsl:value-of select="old_hmrc_payroll_id"/>
                            </OldPayrollId>
                        </xsl:if>
                    </PayIdChgd>
                </xsl:if>

                <xsl:if test="boolean(pay_non_individual_flag[string-length(text())>0])">
                    <PaymentToANonIndividual>
                        <xsl:value-of select="pay_non_individual_flag"/>
                    </PaymentToANonIndividual>
                </xsl:if>
                <xsl:if test="boolean(irregular_employment_flag[string-length(text())>0])">
                    <IrrEmp>
                        <xsl:value-of select="irregular_employment_flag"/>
                    </IrrEmp>
                </xsl:if>
                <xsl:if test="boolean(fund_end_date[string-length(text())>0])">
                    <LeavingDate>
                        <xsl:value-of select="substring(fund_end_date,0,11)"/>
                    </LeavingDate>
                </xsl:if>

                <FiguresToDate>
                    <TaxablePay><xsl:value-of select="total_payment_ytd"/></TaxablePay>
                    <TotalTax><xsl:value-of select="total_tax_ytd"/></TotalTax>
                    <!-- the StudentLoansTD element is not mandatory in the XSD data definition -->
                    <xsl:if test="boolean(total_loan_payment_ytd[string-length(text())>0])">
                        <StudentLoansTD><xsl:value-of select="total_loan_payment_ytd"/></StudentLoansTD>
                    </xsl:if>
                </FiguresToDate>

                <Payment>
                    <BacsHashCode>
                        <xsl:attribute name="subRef"><xsl:value-of select="sub_ref"/></xsl:attribute>
                        <xsl:attribute name="clntSortCode"><xsl:value-of select="clnt_sort_code"/></xsl:attribute>
                        <xsl:attribute name="rcptSortCode"><xsl:value-of select="rcpt_sort_code"/></xsl:attribute>
                        <xsl:attribute name="bacsPaymentAmount"><xsl:value-of select="bacs_payment_amount"/></xsl:attribute>
                        <xsl:value-of select="bacs_hash_code"/>
                    </BacsHashCode>

                    <PayFreq><xsl:value-of select="payment_frequency"/></PayFreq>
                    <PmtDate><xsl:value-of select="substring(latest_payment_date,0,11)"/></PmtDate>
                    <xsl:if test="boolean(late_paye_reporting_reason[string-length(text())>0])">
                        <LateReason><xsl:value-of select="late_paye_reporting_reason"/></LateReason>
                    </xsl:if>
                    <MonthNo><xsl:value-of select="tax_month_number"/></MonthNo>
                    <PeriodsCovered><xsl:value-of select="payment_periods"/></PeriodsCovered>
                    <xsl:if test="boolean(payment_after_contract_flag[string-length(text())>0])">
                        <PmtAfterLeaving><xsl:value-of select="payment_after_contract_flag"/></PmtAfterLeaving>
                    </xsl:if>
                    <HoursWorked><xsl:value-of select="normal_work_hours"/></HoursWorked>

                    <xsl:element name="TaxCode">
                        <xsl:if test="boolean(tax_basis_non_cumulative[string-length(text())>0])">
                            <xsl:attribute name="BasisNonCumulative"><xsl:value-of select="tax_basis_non_cumulative"/></xsl:attribute>
                        </xsl:if>
                        <xsl:if test="boolean(tax_regime[string-length(text())>0])">
                            <xsl:attribute name="TaxRegime"><xsl:value-of select="tax_regime"/></xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="tax_code"/>
                    </xsl:element>

                    <TaxablePay><xsl:value-of select="total_payment_amount"/></TaxablePay>
                    <xsl:if test="boolean(total_non_tax_or_nic_amount[string-length(text())>0])">
                        <NonTaxOrNICPmt><xsl:value-of select="total_non_tax_or_nic_amount"/></NonTaxOrNICPmt>
                    </xsl:if>
                    <xsl:if test="boolean(total_net_deductions[string-length(text())>0])">
                        <DednsFromNetPay><xsl:value-of select="total_net_deductions"/></DednsFromNetPay>
                    </xsl:if>
                    <xsl:if test="boolean(total_pay_after_statutory[string-length(text())>0])">
                        <PayAfterStatDedns><xsl:value-of select="total_pay_after_statutory"/></PayAfterStatDedns>
                    </xsl:if>
                    <xsl:if test="boolean(total_loan_payment[string-length(text())>0])">
                        <StudentLoanRecovered><xsl:value-of select="total_loan_payment"/></StudentLoanRecovered>
                    </xsl:if>
                    <TaxDeductedOrRefunded><xsl:value-of select="total_tax_amount"/></TaxDeductedOrRefunded>

                    <xsl:if test="boolean(trivial_commutation_type [string-length(text())>0])">
                        <xsl:element name="TrivialCommutationPayment">
                            <xsl:attribute name="type"><xsl:value-of select="trivial_commutation_type"/></xsl:attribute>
                            <xsl:value-of select="total_trivial_payment"/>
                        </xsl:element>
                    </xsl:if>

                    <xsl:if test="boolean(flexibly_accessing_pension[string-length(text())>0])
                               or boolean(pension_death_benefit_flag[string-length(text())>0])
                               or boolean(serious_ill_health_ls_flag[string-length(text())>0])   ">
                        <xsl:if test="boolean(total_flexi_taxable_payment[string-length(text())>0])
                                   or boolean(total_flexi_non_tax_payment[string-length(text())>0]) ">
                            <FlexibleDrawdown>
                                <xsl:if test="boolean(flexibly_accessing_pension[string-length(text())>0])">
                                    <FlexiblyAccessingPensionRights><xsl:value-of select="flexibly_accessing_pension"/></FlexiblyAccessingPensionRights>
                                </xsl:if>
                                <xsl:if test="boolean(pension_death_benefit_flag[string-length(text())>0])">
                                    <PensionDeathBenefit><xsl:value-of select="pension_death_benefit_flag"/></PensionDeathBenefit>
                                </xsl:if>
                                <xsl:if test="boolean(serious_ill_health_ls_flag[string-length(text())>0])">
                                    <SeriousIllHealthLumpSum><xsl:value-of select="serious_ill_health_ls_flag"/></SeriousIllHealthLumpSum>
                                </xsl:if>
                                <xsl:if test="boolean(total_flexi_taxable_payment[string-length(text())>0])">
                                    <TaxablePayment><xsl:value-of select="total_flexi_taxable_payment"/></TaxablePayment>
                                </xsl:if>
                                <xsl:if test="boolean(total_flexi_non_tax_payment[string-length(text())>0])">
                                    <NontaxablePayment><xsl:value-of select="total_flexi_non_tax_payment"/></NontaxablePayment>
                                </xsl:if>
                            </FlexibleDrawdown>
                        </xsl:if>
                    </xsl:if>

                </Payment>
            </Employment>
        </Employee>
    </xsl:template>

    <!--
        The first character of address line must be alphabetical or numerical character. Remove the character if it is not.
    -->
    <xsl:template name="CheckLeadingCharacter">
        <xsl:param name="addressline"/>
        <xsl:variable name="legal_characters" select="'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'"/>
        <xsl:choose>
            <xsl:when test="contains($legal_characters,substring($addressline, 1, 1))">
                <xsl:value-of select="$addressline"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="CheckLeadingCharacter">
                    <xsl:with-param name="addressline" select="substring($addressline,2)" />
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>