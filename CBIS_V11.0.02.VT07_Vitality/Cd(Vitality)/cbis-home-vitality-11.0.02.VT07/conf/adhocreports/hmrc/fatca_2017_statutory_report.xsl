<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns="http://hmrc.gov.uk/AEOIUKSubmissionFIReport"> <!-- must be the same as top name space of result -->
    <xsl:output method="xml" indent="yes"/>
    <xsl:template match="fatca">
        <!-- <xsl:copy-of select="."/>  -->   <!-- for testing, just copy to the output the XML returned by MCMapper -->
        <AEOIUKSubmissionFIReport xmlns="http://hmrc.gov.uk/AEOIUKSubmissionFIReport"
                                   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                                   SchemaVersion="2.0"
                                   xsi:schemaLocation="http://hmrc.gov.uk/AEOIUKSubmissionFIReport">
            <MessageData>
                <MessageCategory><xsl:value-of select="FatcaHeader/header/message_category" /></MessageCategory>
                <XMLTimeStamp><xsl:value-of select="FatcaHeader/header/xml_timestamp" /></XMLTimeStamp>
                <AEOIUserId><xsl:value-of select="FatcaHeader/header/fatca_user_id" /></AEOIUserId>
            </MessageData>
            <xsl:choose>
                <xsl:when test="FatcaHeader/header/message_category = 'SubmissionVoid'">
                    <VoidSubmission>
                        <MessageRef><xsl:value-of select="FatcaHeader/header/message_reference" /></MessageRef>
                        <VoidMessageRef><xsl:value-of select="FatcaHeader/header/void_message_reference" /></VoidMessageRef>
                    </VoidSubmission>
                </xsl:when>
                <xsl:otherwise>
                    <Submission>
                        <ReportingPeriod><xsl:value-of select="ReportingInstitution/institution/reporting_period" /></ReportingPeriod>
                        <xsl:choose>
                            <xsl:when test="FatcaHeader/header/message_category = 'SubmissionReplacement'">
                                <Replacement>
                                    <MessageRef><xsl:value-of select="FatcaHeader/header/message_reference" /></MessageRef>
                                    <ReplacedMessageRef><xsl:value-of select="FatcaHeader/header/replaced_message_reference" /></ReplacedMessageRef>
                                </Replacement>
                            </xsl:when>
                            <xsl:otherwise>
                                <MessageRef><xsl:value-of select="FatcaHeader/header/message_reference" /></MessageRef>
                            </xsl:otherwise>
                        </xsl:choose>
                        <FIReturn>
                            <FIReturnAction>
                                <FIReturnRef><xsl:value-of select="ReportingInstitution/institution/return_reference" /></FIReturnRef>
                                <Action><xsl:value-of select="ReportingInstitution/institution/action_type" /></Action>
                            </FIReturnAction>
                            <FIRegisterId><xsl:value-of select="ReportingInstitution/institution/registration_id" /></FIRegisterId>
                            <DueDiligenceInd><xsl:value-of select="ReportingInstitution/institution/due_diligence_ind" /></DueDiligenceInd>
                            <ThresholdInd><xsl:value-of select="ReportingInstitution/institution/threshold_ind" /></ThresholdInd>
                            <InsuranceElection><xsl:value-of select="ReportingInstitution/institution/insurance_election" /></InsuranceElection>
                            <DormantAccElection><xsl:value-of select="ReportingInstitution/institution/dormant_acc_election" /></DormantAccElection>
                            <xsl:choose>
                                <xsl:when test="ReportingInstitution/institution/nil_return_flag = 'Y'">
                                    <NilReturn>
                                    </NilReturn>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:for-each select="FatcaAccountData/account">
                                        <xsl:call-template name="OneAccount" ></xsl:call-template>
                                    </xsl:for-each>
                                </xsl:otherwise>
                            </xsl:choose>
                        </FIReturn>
                    </Submission>
                </xsl:otherwise>
            </xsl:choose>
        </AEOIUKSubmissionFIReport>
    </xsl:template>

    <xsl:template name="OneAccount" >  <!-- mode="copy" ?? -->
        <AccountData>
            <AccountAction>
                <AccountRef><xsl:value-of select="account_reference"/></AccountRef>
                <Action><xsl:value-of select="account_action"/></Action>
            </AccountAction>

            <!-- we don't use the AccountNumber appearing in the HMRC samples.
                 We use AccountIdentifier instead, because our accounts don't fall into the categories of bank accounts. -->

            <AccountIdentifier><xsl:value-of select="account_identifier"/></AccountIdentifier>
            <xsl:element name="AccountBalance">
                <xsl:attribute name="currCode"><xsl:value-of select="currency_code"/></xsl:attribute>
                <xsl:value-of select="account_balance"/>
            </xsl:element>
            <AccountClosedInd><xsl:value-of select="account_closed_ind"/></AccountClosedInd>
            <xsl:for-each select="PaymentData/payment">
                <xsl:call-template name="OnePayment" ></xsl:call-template>
            </xsl:for-each>
            <AccountHolderType><xsl:value-of select="account_holder_type"/></AccountHolderType>

            <xsl:choose>
                <xsl:when test="starts-with(account_data_type,'I')" >
                    <xsl:if test="boolean(last_name[string-length(text())>0])">
                        <Person>
                            <FirstName><xsl:value-of select="first_name"/></FirstName>
                            <LastName><xsl:value-of select="last_name"/></LastName>
                            <xsl:call-template name="OneAddress" ></xsl:call-template>
                            <xsl:for-each select="PersonTaxInfo/person_taxinfo">
                                <xsl:call-template name="PersonTaxInfo" ></xsl:call-template>
                            </xsl:for-each>
                            <xsl:if test="boolean(birth_date[string-length(text())>0])">
                                <BirthInfo>
                                    <BirthDate><xsl:value-of select="birth_date"/></BirthDate>
                                </BirthInfo>
                            </xsl:if>
                        </Person>
                    </xsl:if>
                </xsl:when>
                <xsl:otherwise>
                    <Organisation>
                        <OrganisationName><xsl:value-of select="last_name"/></OrganisationName>
                        <xsl:call-template name="OneAddress" ></xsl:call-template>
                        <xsl:for-each select="OrganisationTaxInfo/organisation_taxinfo">
                            <xsl:call-template name="OrgTaxInfo" ></xsl:call-template>
                        </xsl:for-each>
                        <xsl:for-each select="ControllingPerson/controlling">
                            <xsl:call-template name="OneControlling" ></xsl:call-template>
                        </xsl:for-each>
                    </Organisation>
                </xsl:otherwise>
            </xsl:choose>
        </AccountData>
    </xsl:template>

    <xsl:template name="OnePayment" >
        <xsl:if test="boolean(payment_amount[string-length(text())>0])">
            <PaymentData>
                <PaymentCode><xsl:value-of select="payment_code"/></PaymentCode>
                <xsl:element name="PaymentAmount">
                    <xsl:attribute name="currCode"><xsl:value-of select="currency_code"/></xsl:attribute>
                    <xsl:value-of select="payment_amount"/>
                </xsl:element>
            </PaymentData>
        </xsl:if>
    </xsl:template>

    <xsl:template name="OneControlling" >
        <ControllingPerson>
            <Person>
                <FirstName><xsl:value-of select="first_name"/></FirstName>
                <LastName><xsl:value-of select="last_name"/></LastName>
                <xsl:call-template name="OneAddress" ></xsl:call-template>
                <xsl:for-each select="ControllingTaxInfo/controlling_taxinfo">
                    <xsl:call-template name="PersonTaxInfo" ></xsl:call-template>
                </xsl:for-each>
                <xsl:if test="boolean(birth_date[string-length(text())>0])">
                    <BirthInfo>
                        <BirthDate><xsl:value-of select="birth_date"/></BirthDate>
                    </BirthInfo>
                </xsl:if>
            </Person>
            <xsl:if test="boolean(last_name[string-length(text())>0])">
                <CtrlgPersonType><xsl:value-of select="controlling_person_type"/></CtrlgPersonType>
            </xsl:if>
        </ControllingPerson>
    </xsl:template>

    <xsl:template name="OneAddress" >
        <xsl:if test="boolean(city       [string-length(text())>0])
                                       or boolean(postcode   [string-length(text())>0])
                                       or boolean(street_name[string-length(text())>0])
                                       or boolean(district   [string-length(text())>0])">
            <Address>
                <xsl:if test="boolean(building_identifier[string-length(text())>0])">
                    <BuildingIdentifier><xsl:value-of select="building_identifier"/></BuildingIdentifier>
                </xsl:if>
                <xsl:if test="boolean(street_name[string-length(text())>0])">
                    <StreetName><xsl:value-of select="street_name"/></StreetName>
                </xsl:if>
                <xsl:if test="boolean(district[string-length(text())>0])">
                    <DistrictName><xsl:value-of select="district"/></DistrictName>
                </xsl:if>
                <xsl:if test="boolean(city[string-length(text())>0])">
                    <City><xsl:value-of select="city"/></City>
                </xsl:if>
                <xsl:if test="boolean(postcode[string-length(text())>0])">
                    <PostCode><xsl:value-of select="postcode"/></PostCode>
                </xsl:if>
                <xsl:if test="boolean(country_code[string-length(text())>0])">
                    <CountryCode><xsl:value-of select="country_code"/></CountryCode>
                </xsl:if>
            </Address>
        </xsl:if>
    </xsl:template>

    <xsl:template name="PersonTaxInfo" >
        <xsl:if test="boolean(res_country_code[string-length(text())>0])">
            <HolderTaxInfo>

                <xsl:if test="boolean(tin[string-length(text())>0])">
                    <xsl:choose>
                        <xsl:when test="boolean(issued_by[string-length(text())>0])">
                            <xsl:element name="TIN">
                                <xsl:attribute name="issuedBy"><xsl:value-of select="issued_by"/></xsl:attribute>
                                <xsl:value-of select="tin"/>
                            </xsl:element>
                        </xsl:when>
                        <xsl:otherwise>
                            <TIN><xsl:value-of select="tin"/></TIN>
                        </xsl:otherwise>
                    </xsl:choose>

                </xsl:if>

                <ResCountryCode><xsl:value-of select="res_country_code"/></ResCountryCode>
            </HolderTaxInfo>
        </xsl:if>
    </xsl:template>

    <xsl:template name="OrgTaxInfo" >
        <xsl:if test="boolean(res_country_code[string-length(text())>0])">
            <HolderInfo>

                <xsl:if test="boolean(tin[string-length(text())>0])">
                    <xsl:choose>
                        <xsl:when test="boolean(issued_by[string-length(text())>0])">
                            <xsl:choose>
                                <xsl:when test="boolean(tin_type[string-length(text())>0])">
                                    <xsl:element name="IN">
                                        <xsl:attribute name="issuedBy"><xsl:value-of select="issued_by"/></xsl:attribute>
                                        <xsl:attribute name="INType"><xsl:value-of select="tin_type"/></xsl:attribute>
                                        <xsl:value-of select="tin"/>
                                    </xsl:element>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:element name="IN">
                                        <xsl:attribute name="issuedBy"><xsl:value-of select="issued_by"/></xsl:attribute>
                                        <xsl:value-of select="tin"/>
                                    </xsl:element>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:choose>
                                <xsl:when test="boolean(tin_type[string-length(text())>0])">
                                    <xsl:element name="IN">
                                        <xsl:attribute name="INType"><xsl:value-of select="tin_type"/></xsl:attribute>
                                        <xsl:value-of select="tin"/>
                                    </xsl:element>
                                </xsl:when>
                                <xsl:otherwise>
                                    <IN><xsl:value-of select="tin"/></IN>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:if>

                <ResCountryCode><xsl:value-of select="res_country_code"/></ResCountryCode>
            </HolderInfo>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>
